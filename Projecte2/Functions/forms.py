from django import forms

class BirthdayForm(forms.Form):
    day = forms.IntegerField(label="day")
    month = forms.CharField(label="month")
    year = forms.IntegerField(label="year")

class SignUpTestForm(forms.Form):
    user = forms.CharField(label="Nom d'usuari")
    password = forms.CharField(label="Contrasenya", widget=forms.PasswordInput())
    confirmation = forms.CharField(label="Repeteix contrasenya", widget=forms.PasswordInput())
    email = forms.CharField(label="Email")
