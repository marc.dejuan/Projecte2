# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.views import View
from Functions.forms import BirthdayForm, SignUpTestForm
import re


class HomeView(View):
    def get(self,request):
        """
        Esta función devuelve el home de mi página
        """
        context = {
        }
        return render(request, 'functions/home.html')

class BirthdayView(View):

    def get(self,request):
        error_message=''
        form = BirthdayForm
        context = {
            'error': error_message,
            'birthday_form': form

        }
        return render(request, 'functions/birthday.html', context)

    def post(self, request):
        error_message = ''
        form = BirthdayForm(request.POST)
        if request.method == 'POST':
            DAY=request.POST.get('day')
            MONTH = request.POST.get('month')
            YEAR = request.POST.get('year')
            user_month = valid_month(month=MONTH)
            user_day = valid_day(day=DAY)
            user_year = valid_year(YEAR)
            if (user_month and user_day and user_year):
                error_message=("Data correcta")
            else:
                error_message=("Data incorrecta")

        context={
            'error': error_message,
            'birthday_form': form
        }
        return render(request, 'functions/birthday.html', context)


class Rot13View(View):
    def get(self,request):
        return render(request,'functions/rot13.html')

    def post(self, request):
        rot13 = ''
        text = request.POST.get('text')
        if text:
            rot13 = text.encode('rot13')
        context={
            'text': rot13
        }
        return render(request, 'functions/rot13.html', context)

class SignUpTestView(View):

    def get(self,request):
        form = SignUpTestForm
        context = {
            'signuptest_form': form
        }
        return render(request, 'functions/SignUpTest.html', context)

    def post(self,request):
        error1=''
        error2=''
        error3=''
        error4=''
        have_error=False

        form = SignUpTestForm(request.POST)
        if request.method == 'POST':
            USERNAME = request.POST.get('user')
            PASSWORD = request.POST.get('password')
            CONFIRMATION = request.POST.get('confirmation')
            EMAIL = request.POST.get('email')

            if not valid_username(USERNAME):
                error1 = "No és un nom d'usuari valid"
                have_error = True

            if not valid_password(PASSWORD):
                error2 = u"No és una contrasenya correcta"
                have_error = True

            elif PASSWORD != CONFIRMATION:
                error3 = u"Les contrasenyes no coincideixen"
                have_error = True

            if not valid_email(EMAIL):
                error4 = u"No es un email valid"
                have_error = True

            if have_error==False:
                error1=u"Et podries fer l'usuari"

        context = {
            'error1': error1,
            'error2': error2,
            'error3': error3,
            'error4': error4,
            'signuptest_form': form
        }
        return render(request, 'functions/SignUpTest.html', context)



USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return username and USER_RE.match(username)

PASS_RE = re.compile(r"^.{3,20}$")
def valid_password(password):
    return password and PASS_RE.match(password)

EMAIL_RE = re.compile(r"^[\S]+@[\S]+.[\S]+$")
def valid_email(email):
    return email and PASS_RE.match(email)

def encode(text):
    res = text.maketrans(
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM")
    return res

def valid_day(day):
    if day.isdigit() and int(day) in range(1, 32):
        return int(day)

def valid_month(month):
    months = {'jan': 'January', 'feb': 'February', 'mar': 'March', 'apr': 'April', 'may': 'May',
              'jun': 'June', 'jul': 'July', 'aug': 'August', 'sep': 'September',
              'oct': 'October', 'nov': 'November', 'dec': 'December'}
    m = month.lower()[:3]
    if m in months:
        return months[m]

def valid_year(year):
    if year.isdigit() and int(year) in range(1900, 2021):
        return year