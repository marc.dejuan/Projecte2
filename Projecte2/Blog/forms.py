from django import forms
from Blog.models import Posts
from django.core.exceptions import ValidationError


class PostForm(forms.ModelForm):

    class Meta:
        model=Posts
        fields = ['subject', 'text']
