# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import django.core.cache.backends
from django.core.cache import cache
from django.http import HttpResponseNotFound
from django.shortcuts import render, redirect
from django.urls import reverse
# Create your views here.
from django.views import View
from datetime import datetime, timedelta

from Blog.forms import PostForm
from Blog.models import Posts
import memcache
mc = memcache.Client(['127.0.0.1:8001'], debug=0)





class HomeBlogView(View):
    def get(self, request):
        posts, age = get_posts()
        context = {
            'post_list': posts,
            'age': age_str(age)
        }
        return render(request, 'blog/home.html', context)

class DetailView(View):

    def get(self, request, pk):
        possible_posts=Posts.objects.filter(pk=pk)
        posts = possible_posts[0] if len(possible_posts) > 0 else None

        post_key='POST_'+str(posts.pk)
        val, age = age_get(post_key)

        if not val:
            val=posts
            age_set(post_key, val)
            age=0

        if posts is not None:
            # cargar la plantilla de detalle
            context = {
                'posts': posts,
                'age': age_str(age)
            }
            return render(request, 'blog/detail.html', context)
        else:
            return HttpResponseNotFound('No existe la foto')  # 404 not found

class CreateView(View):

    def get(self, request):

        success_message='' #sempre estarà buit per que es get
        form = PostForm()
        context={
            'form': form,
            'success_message': success_message
        }
        return render(request, 'blog/new_post.html', context)


    def post(self, request):

        success_message = ''
        post = Posts()
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            new_post = form.save()  # guarda l'objecte i el retornes
            # form = PostForm()
            # success_message = 'Guardat amb èxit! '
            # success_message += '<a href="{0}">'.format(reverse('post_detail', args=[new_post.pk]))
            # success_message += 'Veure publicació'
            # success_message += '</a>'
            return redirect('post_detail', new_post.pk)
            # context = {
            #     'form': form,
            #     'success_message': success_message
            # }

def age_set(key, val):
    save_time=datetime.utcnow() #temps actual
    mc.set(key, val, save_time) #guarda una tupla amb la clau, valor i temps


def age_get(key):
    r = mc.get(key)
    if r:
        val, save_time = r
        age = (datetime.utcnow() - save_time).total_seconds()
    else:
        val, age = None, 0
    return val, age

# def add_post(ip,post):
#     post.put()
#     get_posts(update=True)
#     return str(post.key().id())

def get_posts(update = False):
    q=Posts.objects.order_by('-created_at')
    p=q[:10]
    key = 'HomeBlog'

    posts, age = age_get(key)
    if update or posts is None:
        posts=list(p)
        age_set(key, posts)

    return posts, age

def age_str(age):
    age = int(age)
    s = 'queried fa %s segons'

    return s % age